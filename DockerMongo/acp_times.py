from __future__ import division
"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow
import math


#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.
#

ACP_MIN = [(0,15), (200, 15), (400, 15), (600, 15), (1000, 11.428), (1300, 13.333)]

ACP_MAX = [(0, 34), (200, 34), (400, 32), (600, 30), (1000, 28), (1300, 26)]



def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
    """
    
    if(abs(control_dist_km-brevet_dist_km)<10):
        control_dist_km = brevet_dist_km
    i = 1
    time = 0
    if((ACP_MAX[1][0] < control_dist_km)):
        while(ACP_MAX[i][0] <= control_dist_km):
            time += (ACP_MAX[i][0]-ACP_MAX[i-1][0])/ACP_MAX[i][1]
            i+=1
        time += (control_dist_km - ACP_MAX[i-1][0])/ACP_MAX[i][1]
    else:
        time += control_dist_km/ACP_MAX[0][1]

    
    time_h = int(time)
    time_min = round((time-time_h)*60)

    time = arrow.get(brevet_start_time)
    time_shifted = time.shift(hours=time_h, minutes=time_min)

    return time_shifted.isoformat()


def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    if(abs(control_dist_km-brevet_dist_km)<10):
        control_dist_km = brevet_dist_km
    i = 1
    time = 0
    if((ACP_MIN[1][0] <= control_dist_km)):
        while(ACP_MIN[i][0] <= control_dist_km):
            time += (ACP_MIN[i][0]-ACP_MIN[i-1][0])/ACP_MIN[i][1]
            i+=1
        time += (control_dist_km - ACP_MIN[i-1][0])/ACP_MIN[i][1]
    else:
        time += control_dist_km/ACP_MIN[0][1]
    
    time_h = int(time)
    time_min = round((time-time_h)*60)
    
    time = arrow.get(brevet_start_time)
    time_shifted = time.shift(hours=time_h, minutes=time_min)

    return time_shifted.isoformat()
