"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""
import os
import flask
from flask import request
from pymongo import MongoClient
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config

import logging

###
# Globals
###
client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.tododb

app = flask.Flask(__name__)
CONFIG = config.configuration()
app.secret_key = "SECRET KEY"

db_array=[]
###
# Pages
###


@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')

@app.route('/sub')
def sub():
    app.logger.debug("Entered submit function")
    for item in db_array:
        db.tododb.insert_one(item)
    return "nothing"

@app.route("/display", methods=["POST"])
def display():
    app.logger.debug("Entered display function")
    _items = db.tododb.find()
    items = [item for item in _items]
    app.logger.debug("Printing items: {}".format(items))
    return flask.render_template('display.html', items=items)
 

@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    distance = request.args.get('distance', 200, type=float)
    beginDate = request.args.get('begindate', str)
    beginTime = request.args.get('begintime', str)
    beginDate = beginDate + " " + beginTime
    app.logger.debug("km={}".format(km))
    app.logger.debug("dist={}".format(distance))
    app.logger.debug("time={}".format(beginDate))

    app.logger.debug("request.args: {}".format(request.args))
    open_time = acp_times.open_time(km, distance, beginDate)
    close_time = acp_times.close_time(km, distance, beginDate)
    item_dict = {"km": km, "distance": distance, "beginDate": beginDate, "open":open_time, "close": close_time  }
    app.logger.debug("item_dict: {}".format(item_dict))
    db_array.append(item_dict)
    result = {"open": open_time, "close": close_time}
    return flask.jsonify(result=result)


#############

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0", debug=True)
